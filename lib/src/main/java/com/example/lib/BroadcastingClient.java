package com.example.lib;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

public class BroadcastingClient {
    private static DatagramSocket socket = null;
    static ArrayList<InetAddress> broadcastArrayList = new ArrayList();
    public static void main(String[] args) throws IOException

    {
        String message = "MicServer9191";
        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
        for (NetworkInterface netint : Collections.list(nets))
            displayInterfaceInformation(netint);
//        new MulticastReceiver().start();

        while (true) {
            try {
                for(InetAddress inetAddress : broadcastArrayList) {
                    broadcast(message, inetAddress);
                }
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {

        System.out.printf("Display name: %s\n", netint.getDisplayName());
        System.out.printf("Name: %s\n", netint.getName());
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();

        for (InterfaceAddress interfaceAddress : netint.getInterfaceAddresses()) {

            InetAddress broadcastAddress = interfaceAddress.getBroadcast();
            System.out.printf("Broadcast Address: %s\n", broadcastAddress);
            if(broadcastAddress != null)
                broadcastArrayList.add(interfaceAddress.getBroadcast());
        }
        System.out.printf("\n");
    }

    public static void broadcast(
            String broadcastMessage, InetAddress address) throws IOException {
        socket = new DatagramSocket();
        socket.setBroadcast(true);

        byte[] buffer = broadcastMessage.getBytes();
        System.out.println("Broadcasting to " + address);
        DatagramPacket packet
                = new DatagramPacket(buffer, buffer.length, address, 9191);
        socket.send(packet);
        socket.close();
    }

}
