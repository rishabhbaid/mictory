package com.example.lib;

import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.metal.MetalLookAndFeel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ListDialog {
    private JList list;
    private JLabel label;
    private JOptionPane optionPane;
    private JButton okButton, cancelButton, allowButton, resetButton;
    private ActionListener okEvent, cancelEvent, allowEvent, resetEvent;
    private JDialog dialog;
    private static final String titleMessage = "List of speakers";

    public ListDialog(String message, JList listToDisplay){
        list = listToDisplay;
        label = new JLabel(message);
        createAndDisplayOptionPane();
    }

    public ListDialog(String title, String message, JList listToDisplay){
        this(message, listToDisplay);
        dialog.setTitle(title);
        dialog.setBackground(Color.BLUE);
    }

    private void createAndDisplayOptionPane(){
        JFrame frame = new JFrame("Smart Mic");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
        setupButtons();
        JPanel pane = layoutComponents();
       // pane.setBorder(BorderFactory.createEtchedBorder());
        pane.setAutoscrolls(true);
        optionPane = new JOptionPane(pane);
        optionPane.setOptions(new Object[]{cancelButton, allowButton, resetButton});
        //dialog = optionPane.createDialog(titleMessage);
        //dialog.setBackground(Color.BLUE);
        frame.setContentPane(optionPane);

        try {
            // Set cross-platform Java L&F (also called "Metal")
            MetalLookAndFeel.setCurrentTheme(new MyDefaultMetalTheme());

            SwingUtilities.updateComponentTreeUI(frame);
            UIManager.setLookAndFeel(
                    new MetalLookAndFeel());
        }
        catch (UnsupportedLookAndFeelException e) {
            // handle exception
        }

        //Display the window.
        frame.pack();
        frame.setVisible(true);

    }

    private void setupButtons(){
//        okButton = new JButton("Ok");
//        okButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                ListDialog.this.handleOkButtonClick(e);
//            }
//        });

        cancelButton = new JButton("Quit");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ListDialog.this.handleCancelButtonClick(e);
            }
        });

        allowButton = new JButton("Allow");
        allowButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    ListDialog.this.handleAllowButtonClick(e);
            }
        });

        resetButton = new JButton("Reset");
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ListDialog.this.handleresetButtonClick(e);
            }
        });
    }

    private JPanel layoutComponents(){
        centerListElements();
        JPanel panel = new JPanel(new BorderLayout(20, 20));
        panel.add(label, BorderLayout.NORTH);
        panel.add(list, BorderLayout.CENTER);
        panel.setPreferredSize(new Dimension(640, 480));
       // panel.setBackground(Color.);
        return panel;
    }

    private void centerListElements(){
        DefaultListCellRenderer renderer = (DefaultListCellRenderer) list.getCellRenderer();
        renderer.setHorizontalAlignment(SwingConstants.CENTER);
    }

    public void setOnOk(ActionListener event){ okEvent = event; }

    public void setOnClose(ActionListener event){
        cancelEvent  = event;
    }

    public void setOnAllow(ActionListener event){
        allowEvent  = event;
    }

    public void setOnreset(ActionListener event){
        resetEvent  = event;
    }



    private void handleOkButtonClick(ActionEvent e){
        if(okEvent != null){ okEvent.actionPerformed(e); }
        hide();
    }

    private void handleCancelButtonClick(ActionEvent e){
        if(cancelEvent != null){ cancelEvent.actionPerformed(e);}
        hide();
    }

    private void handleAllowButtonClick(ActionEvent e){
        if(allowEvent != null){ allowEvent.actionPerformed(e);}
       // hide();
    }

    private void handleresetButtonClick(ActionEvent e){
        if(resetEvent != null){ resetEvent.actionPerformed(e);}
      //  hide();
    }

    public void show() {
        if(dialog != null)
             dialog.setVisible(true);
    }

    private void hide(){
        dialog.hide();
        dialog.removeAll();
    }

    public Object getSelectedItem(){ return list.getSelectedValue(); }
}