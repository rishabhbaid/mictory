package com.example.lib;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.FileSystems;
import java.nio.file.Path;


import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.DefaultListModel;
import javax.swing.JList;

// No multiple scan for qr code
// topic input option
// Change GET IP in android...

public class TicketServer {
    static final String listHelpMessage = "Name---IP address of client";
    static final String allowMessageForClient = "ACK";
    private final static int client_port = 50008;
    static int port = 50009;
    private static DatagramSocket socket = null;
    final static DefaultListModel model = getModel();
    static Thread thread, streamingThread;
    static int sosCount = 0;
    static int sosCountThreshold = 1;

    protected static void sendMessageToClient(InetAddress inetAddress, byte[] buf) {
        try {
            DatagramPacket packet = new DatagramPacket(buf, buf.length,
                    inetAddress, client_port);
            socket = new DatagramSocket();
            socket.send(packet);
            System.out.println("********** Packet send to IP: ***************" + inetAddress.getHostAddress());
        } catch(IOException io) {
            System.out.println("Error in sending message.");
        } catch (NullPointerException ne) {
            System.out.println("Null pointer exception" + inetAddress);
        }
    }

    protected static String getMessage(DatagramPacket packet) {
        String msg = "";
        byte[] data = packet.getData();
        for(int i = 0; i < packet.getLength(); i++) {
            char c = (char)data[i];
            msg += c;
            System.out.print(c);
        }
        return msg;
    }

    private static DefaultListModel getModel() {
        QueueManager queueManager = QueueManager.getInstance();
        String[] allQueuedSpeakersName = queueManager.getAllQueuedSpeakers();
        InetAddress[] allQueuedSpeakersIP = queueManager.getAllQueuedSpeakersInetAddress();
        String[] allQueuedSpeakers = new String[allQueuedSpeakersIP.length];
        final DefaultListModel model = new DefaultListModel();
        for(int i = 0; i < allQueuedSpeakersName.length; i++) {
            allQueuedSpeakers[i] = allQueuedSpeakersName[i] + "::" + allQueuedSpeakersIP[i];
            System.out.println("Speakers: " + allQueuedSpeakers[i]);
            model.addElement(allQueuedSpeakers[i]);
        }
        return model;
    }

    private static void setUpAppletProperties() {
        final QueueManager queueManager = QueueManager.getInstance();
        JList list = new JList(model);
        final ListDialog dialog = new ListDialog(listHelpMessage, list);
        dialog.setOnOk(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Chosen item: " + dialog.getSelectedItem());
            }
        });

        dialog.setOnClose(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println("on close");
                thread.interrupt();
                System.exit(0);
            }
        });
        dialog.setOnAllow(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    System.out.println("Allowing following speaker: " + dialog.getSelectedItem());
                    String speaker = dialog.getSelectedItem().toString();
                    String speakerIP = speaker.split("/")[1];
                    System.out.println("Speakers IP:" + speakerIP);
                    byte[] data = allowMessageForClient.getBytes();
                    InetAddress ip = InetAddress.getByName(speakerIP.trim());
                    TicketServer.sendMessageToClient(ip, data);
                    queueManager.setAllowedClientIP(ip);
                    queueManager.removeFromQueue(ip);
                    model.removeElement(dialog.getSelectedItem());
                } catch(Exception exception) {
                    System.out.println("Exception" + exception.getLocalizedMessage());
                }
            }
        });


        dialog.setOnreset(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("reset list: " + dialog.getSelectedItem());
                model.removeAllElements();
                queueManager.clearQueue();
                TicketServer.sosCount = 0;
                queueManager.setAllowedClientIP(null);
//                streamingThread.interrupt();
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e1) {
//                    e1.printStackTrace();
//                }
//                Server server = new Server();
//                streamingThread = new Thread(server);
//                streamingThread.start();

            }
        });
        dialog.show();
    }

    protected static String getPacketType(DatagramPacket packet) {
        String message = getMessage(packet);
        String sosMarker = "SOS:";
        String sosType = "SOS";
        String removeFromQueueMarker = "PULLOUT:";
        String removeFromQueueOperationType = "PULLOUT";
        String questionType = "QUESTION";
        if(message.contains(sosMarker)) {
            return sosType;
        } else if(message.contains(removeFromQueueMarker)) {
            return removeFromQueueOperationType;
        } else {
            return questionType;
        }
    }

    protected static boolean isThresholdReachedForSOS() {
        if(sosCount > sosCountThreshold)
            return true;
        else
            return false;
    }


    public static void main(String args[]) {
        QueueServer queueServer = new QueueServer();
        thread = new Thread(queueServer);
        thread.start();
        Server server = new Server();
        streamingThread = new Thread(server);
        streamingThread.start();
        setUpAppletProperties();
    }
}

class QueueServer implements Runnable {



    public void displayTray(String title, String msg, TrayIcon.MessageType messageType) {
        //Obtain only one instance of the SystemTray object
        SystemTray tray = SystemTray.getSystemTray();

        String fs = File.separator;
        Path path = FileSystems.getDefault().getPath("").toAbsolutePath();
        URL defaultSound = getClass().getResource("/lib/sos.wav");
        String absolutePath = path.toString()  +   fs + "sos.wav";
        System.out.println("Absolute Path: " + absolutePath);
        File file = new File("lib/sos.wav");
        absolutePath = path.toString() + "lib"  + fs + "warning.png";
        System.out.println("Absolute Path: " + absolutePath);
        Image image = Toolkit.getDefaultToolkit().createImage("lib/warning.png");
        //Alternative (if the icon is on the classpath):
        //Image image = Toolkit.getDefaultToolkit().createImage(getClass().getResource("icon.png"));

        TrayIcon trayIcon = new TrayIcon(image, "Tray Demo");
        //Let the system resize the image if needed
        trayIcon.setImageAutoSize(true);
        //Set tooltip text for the tray icon
        trayIcon.setToolTip("System tray icon demo");
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }

        trayIcon.displayMessage(msg, title, messageType);


        URL url = null;
        if (file.canRead()) {
            try {
                url = file.toURI().toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        System.out.println(url);
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        System.out.println("should've played by now");



        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        tray.remove(trayIcon);
    }

    @Override
    public void run() {
        try {
            final QueueManager queueManager = QueueManager.getInstance();
            DatagramSocket serverSocket = new DatagramSocket(TicketServer.port);
            byte[] receiveData = new byte[200];
            String msg = "People are having doubts! Consider pausing!", title = "Alert!";
            TrayIcon.MessageType messageType = TrayIcon.MessageType.ERROR;
            System.out.println("Not reaching here");
            while (true) {
                System.out.println("Started Ticket Server");
                final DatagramPacket receivePacket = new DatagramPacket(receiveData,
                        receiveData.length);
                serverSocket.receive(receivePacket);
                System.out.println( " Packet recevied from: " + receivePacket.getAddress() + " " + TicketServer.port);
                String packetType = TicketServer.getPacketType(receivePacket);
                System.out.println("Packet type: " + packetType);
                if(packetType.equals("SOS")) {
                    TicketServer.sosCount++;
                    if(TicketServer.isThresholdReachedForSOS()) {
                        displayTray(msg, title, messageType);
                        TicketServer.sosCount = 0;
                    }
                } else if(packetType.equals("PULLOUT")) {
                    InetAddress ip = receivePacket.getAddress();
                    String speaker =  queueManager.getNameFromIP(ip) + "::" + receivePacket.getAddress();
                    queueManager.removeFromQueue(ip);
                    System.out.println("Speaker name: " + speaker);
                    TicketServer.model.removeElement(speaker);
//                    while(!TicketServer.model.isEmpty())  {
//                        System.out.println("Model element: " + TicketServer.model.getElementAt(0));
//                        TicketServer.model.removeElementAt(0);
//                    }
                } else {
                    String newSpeaker = TicketServer.getMessage(receivePacket) + "::" + receivePacket.getAddress();
                    InetAddress ip = receivePacket.getAddress();
                    if(queueManager.isSpeakerInQueue(receivePacket.getAddress())) {
                        System.out.println("Speaker already in queue with IP" + ip.getHostAddress());
                    } else {
                        int ticketNumber = queueManager.addToQueueAndGetNumber(receivePacket.getAddress(),
                                TicketServer.getMessage(receivePacket));
                        TicketServer.sendMessageToClient(receivePacket.getAddress(),
                                ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(ticketNumber).array());
                        String nextSpeaker = queueManager.getNameForNextSpeaker();
                        System.out.println("Next speaker: " + nextSpeaker + " " + queueManager.getInetAddressForNextSpeaker());
                        // queueManager.removeFromQueue(queueManager.getInetAddressForNextSpeaker());
                        InetAddress inetAddress = queueManager.getInetAddressForNextSpeaker();
                        System.out.println(inetAddress);

                        System.out.println("Speaker with IP " + receivePacket.getAddress() + " already in queue");
                        TicketServer.model.addElement(newSpeaker);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
