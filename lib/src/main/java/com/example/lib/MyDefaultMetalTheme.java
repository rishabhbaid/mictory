package com.example.lib;

import java.awt.Color;

import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.MetalTheme;

class MyDefaultMetalTheme extends MetalTheme {

    public ColorUIResource getWindowTitleInactiveBackground() {
        return new ColorUIResource(java.awt.Color.blue);
    }

    public ColorUIResource getWindowTitleBackground() {
        return new ColorUIResource(java.awt.Color.blue);
    }

    public ColorUIResource getPrimaryControlHighlight() {
        return new ColorUIResource(java.awt.Color.orange);
    }

    public ColorUIResource getPrimaryControlDarkShadow() {
        return new ColorUIResource(java.awt.Color.blue);
    }

    public ColorUIResource getPrimaryControl() {
        return new ColorUIResource(Color.blue);
    }

    public ColorUIResource getControlHighlight() {
        return new ColorUIResource(java.awt.Color.blue);
    }

    public ColorUIResource getControlDarkShadow() {
        return new ColorUIResource(java.awt.Color.blue);
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    protected ColorUIResource getPrimary1() {
        return null;
    }

    @Override
    protected ColorUIResource getPrimary2() {
        return null;
    }

    @Override
    protected ColorUIResource getPrimary3() {
        return null;
    }

    @Override
    protected ColorUIResource getSecondary1() {
        return null;
    }

    @Override
    protected ColorUIResource getSecondary2() {
        return null;
    }

    @Override
    protected ColorUIResource getSecondary3() {
        return null;
    }

    @Override
    public FontUIResource getControlTextFont() {
        return null;
    }

    @Override
    public FontUIResource getSystemTextFont() {
        return null;
    }

    @Override
    public FontUIResource getUserTextFont() {
        return null;
    }

    @Override
    public FontUIResource getMenuTextFont() {
        return null;
    }

    @Override
    public FontUIResource getWindowTitleFont() {
        return null;
    }

    @Override
    public FontUIResource getSubTextFont() {
        return null;
    }

    public ColorUIResource getControl() {
        return new ColorUIResource(Color.blue);
    }
}
