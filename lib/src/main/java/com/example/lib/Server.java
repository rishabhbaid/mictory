package com.example.lib;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.SourceDataLine;

class Server implements Runnable {

    AudioInputStream audioInputStream;
    static AudioInputStream ais;
    static AudioFormat format;
    static boolean status = true;
    static int port = 50005;
    static int sampleRate = 16000;

    public static void main(String args[]) {


    }

    public static void toSpeaker(byte soundbytes[]) {
        try {

            DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
            SourceDataLine sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);

            sourceDataLine.open(format);

            FloatControl volumeControl = (FloatControl) sourceDataLine.getControl(FloatControl.Type.MASTER_GAIN);
            volumeControl.setValue(6.0206f);

            sourceDataLine.start();
            sourceDataLine.open(format);

            sourceDataLine.start();

            System.out.println("format? :" + sourceDataLine.getFormat());

            sourceDataLine.write(soundbytes, 0, soundbytes.length);
            System.out.println(soundbytes.toString());
            sourceDataLine.drain();
            sourceDataLine.close();
        } catch (Exception e) {
            System.out.println("Not working in speakers...");
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        QueueManager queueManager = QueueManager.getInstance();
        DatagramSocket serverSocket = null;
        try {
            serverSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        byte[] receiveData = new byte[1280];
        // ( 1280 for 16 000Hz and 3584 for 44 100Hz (use AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat) to get the correct size)

        format = new AudioFormat(sampleRate, 16 , 1, true, false);

        while (status == true) {
            System.out.println("running");
            final DatagramPacket receivePacket = new DatagramPacket(receiveData,
                    receiveData.length);

            try {
                serverSocket.receive(receivePacket);
            } catch (IOException e) {
                e.printStackTrace();
            }
            InetAddress ip = receivePacket.getAddress();
            InetAddress allowedIP = queueManager.getAllowedClientIP();
            if(allowedIP == null || !ip.equals(allowedIP)) {
                System.out.println("Client not allowed to speak. Allowed IP:"
                        + allowedIP);
                continue;
            } else {
                ByteArrayInputStream baiss = new ByteArrayInputStream(
                        receivePacket.getData());

                ais = new AudioInputStream(baiss, format, receivePacket.getLength());

                // A thread solve the problem of chunky audio
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        toSpeaker(receivePacket.getData());
                    }
                }).start();
            }
        }
    }
}