package com.example.lib;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;


class UserInfo {
    InetAddress inetAddress;
    String name;
    UserInfo(InetAddress inetAddress, String name) {
        this.inetAddress = inetAddress;
        this.name = name;
    }
}

public class QueueManager {
    private static final QueueManager instance = new QueueManager();
    private static final Map<InetAddress, Integer> hmap = new HashMap<>();
    private static final Map<InetAddress, String> hmapName = new HashMap<>();
    private static final Queue<InetAddress> queue = new LinkedList<>();
    private static final Queue<String> queueName = new LinkedList<>();
    private static int queueNumber = 0;
    private static InetAddress allowedClientIP;

    public  InetAddress getAllowedClientIP() {
        return allowedClientIP;
    }

    public  void setAllowedClientIP(InetAddress allowedClientIP) {
        QueueManager.allowedClientIP = allowedClientIP;
    }

    public static QueueManager getInstance() {
        return instance;
    }

    public int addToQueueAndGetNumber(InetAddress inetAddress, String name) {
        System.out.println("Adding to queue, IP:" + inetAddress.getHostAddress() + " Name:" + name);
        queueNumber += 1;
        queue.add(inetAddress);
        queueName.add(name);
        hmap.put(inetAddress, queueNumber);
        hmapName.put(inetAddress, name);
        return queueNumber;
    }

    public boolean removeFromQueue(InetAddress inetAddress) {
        if(hmap.containsKey(inetAddress) && hmapName.containsKey(inetAddress)) {
            hmap.remove(inetAddress);
            hmapName.remove(inetAddress);
            queue.remove(inetAddress);
            queueName.remove(hmapName.get(inetAddress));
            return true;
        } else {
            return false;
        }
    }

    public InetAddress getInetAddressForNextSpeaker() {
        return queue.peek();
    }

    public String getNameForNextSpeaker() {

        return hmapName.get(queue.peek());
    }

    public String getNameFromIP(InetAddress ip) {
        System.out.println("Hmap size:" + hmapName.size());
        return hmapName.get(ip);
    }

    public String[] getAllQueuedSpeakers() {
        return queueName.toArray(new String[0]);
    }

    public InetAddress[] getAllQueuedSpeakersInetAddress() {
        return queue.toArray(new InetAddress[0]);
    }

    public boolean isSpeakerInQueue(InetAddress inetAddress) {
        System.out.println("Check for IP:" + inetAddress.getHostAddress());
        return hmap.containsKey(inetAddress);
    }

    public boolean clearQueue() {
        hmap.clear();
        hmapName.clear();
        queue.clear();
        queueName.clear();
        return true;
    }
}
