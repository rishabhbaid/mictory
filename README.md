# Smart Mic

The application consists of a server and an android client.
The server code is in the lib directory of app.


### Objectives:

Enable audience members in a conference hall to ask questions without the use of conventional mic.
Bringing unclear parts of the presentation to the presenters attention

### Solution:

An integrated application to seamlessly allow the audience members to use their personal devices (mobile phones/laptops) as mic and also provide the presenter with a way to  authorize such people from the audience.

### Requirements:

Android Device (For Client)
PC/Laptop (For the preserter)
Speakers

### Phases of Development:

##### Stage 1: 
A network of interconnected devices (with audience as client and presenter laptop as server) which will be connected to speaker wirelessly or wired. Fine tuning the system to have good quality audio transmission.

##### Stage 2:
Stage 1 capabilities are extended upon by providing the presenter with the ability to choose which audience member should get the chance to speak.

##### Stage 3:
Fine tuning of the application and addition of a feature to allow clients to send a signal akin to SOS, which signifies that they don’t understand some particular topic. If enough clients press that signal in a given time, then the presenter will get a ping. 

##### Stage 4:
Integrating the application with the TUM Campus Application.


### Application Flow:

* The presenter will start a server application and provide a QR code containing its IP address.
* The audience member will start the TUM Campus app and then go to the smart mic tab to access its functionalities.
* Client sends a request to the server for mic time using “Join queue” button. Info sent: Name, IP
* Server will then acknowledge that the client has been added to queue.
* The server will have a queue visible on its side that shows all the connected users. It will then provide permission to one of the users to ask questions.
* As soon as this happens the button allowing the user to start asking questions gets enabled along with the stop button.
* The user can then press this button to start asking the questions.
* After asking the questions, the user can press the stop button to end his connection.
* Users can also press the “Poke the Prof” button in case they don’t understand a topic. If enough users press this button in a given time then a notification is sent to the professor indicating that a lot of students don’t understand this topic so that appropriate action can be taken.


### Future Work

* Option to add hint text while asking questions could be added. This shall help the presenter to decide whom to allow first.
* Improvements for removing static noise further could be done.
* UI for presenter could be improved further.


